const initialState = {
   nombre: "",
   periodo: "",
   presupuestoInicial: "",
};

const PresupuestoReducer = (state = initialState, action) => {
   switch (action.type) {
      case "ADD_PRESUPUESTO_TOTAL":
         return {
            ...state,
            nombre: action.nombre,
            periodo: action.periodo,
            presupuestoInicial: action.presupuestoInicial,
         };

      default:
         return state;
   }
};

export default PresupuestoReducer;
