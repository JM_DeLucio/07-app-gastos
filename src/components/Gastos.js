import React, { useState, useContext, useEffect } from "react";
import { Button, Container, Form, Grid, Header, Icon, Segment, Table, Message, Popup } from "semantic-ui-react";
import PresupuestoContext from "../context/PresupuestoContext";
import ModalAlert from "./ModalAlert";

const Gastos = () => {
   const { presupuesto, dispatchPresupuesto } = useContext(PresupuestoContext);
   const [lista, setLista] = useState([]);
   const [restantePresupuesto, setRestantePresupuesto] = useState(parseFloat(presupuesto.presupuestoInicial));
   const [modal, setModal] = useState(false);
   const [message, setMessage] = useState({
      mensajeTitle: "",
      mensaje: "",
      accion: "",
   });
   const [data, setData] = useState({
      gasto: "",
      cantidad: "",
   });
   const [msjGastoExcedido, setMsjGastoExcedido] = useState(false);

   const inputHandler = (e) => {
      if (e.target.name === "cantidad") {
         const str = e.target.value.replace(/[^0-9.]/g, "");
         if (str.length <= 9) {
            str.replace(/[^0-9.]/g, "");
            setData((prevState) => ({
               ...prevState,
               [e.target.name]: str.toUpperCase(),
            }));
         }
      } else {
         const str = e.target.value.replace(/[^a-zA-Z0-9 ]/g, "");
         if (str.length <= 45) {
            str.replace(/[^a-zA-Z0-9 ]/g, "");
            setData((prevState) => ({
               ...prevState,
               [e.target.name]: str.toUpperCase(),
            }));
         }
      }
   };

   const onClickHandlerRetornarInicio = () => {
      handlerModal();
      setMessage({
         mensajeTitle: "Cancelar",
         mensaje: "¿Esta seguro perdera todo su presupuesto?",
         accion: "cancelar",
      });
   };

   const handlerModal = () => {
      setModal(!modal);
   };

   const agregarGasto = () => {
      if (data.gasto === "" || data.cantidad === "") {
         handlerModal();
         setMessage({
            mensajeTitle: "Campos Vacíos",
            mensaje: "Favor de llenar los campos solicitados",
            accion: "insertar",
         });
      } else {
         if (parseFloat(data.cantidad) === 0) {
            handlerModal();
            setMessage({
               mensajeTitle: "Cantidad Incorrecta",
               mensaje: "Favor de colocar una cantidad diferente de 0",
               accion: "insertar",
            });
         } else {
            setLista((prevState) => [...prevState, { gasto: data.gasto, cantidad: parseFloat(data.cantidad) }]);
            let ope = parseFloat(restantePresupuesto) - parseFloat(data.cantidad);
            setRestantePresupuesto(ope.toFixed(2));
            limpiarFormulario();
         }
      }
   };

   useEffect(() => {
      if (restantePresupuesto < 0) {
         setMsjGastoExcedido(true);
      }
   }, [restantePresupuesto]);

   const limpiarFormulario = () => {
      setData({
         gasto: "",
         cantidad: "",
      });
   };

   const mostrarColumnas = () => {
      return lista.map((x) => {
         return (
            <>
               <Table.Row>
                  <Table.Cell>{x.gasto}</Table.Cell>
                  <Table.Cell>{x.cantidad}</Table.Cell>
                  <Table.Cell>
                     <Button basic color="red" onClick={() => borrarGasto(x.gasto, x.cantidad)}>
                        Borrar
                     </Button>
                  </Table.Cell>
               </Table.Row>
            </>
         );
      });
   };

   const borrarGasto = (gasto, cantidad) => {
      const newLista = lista;
      const buscarIndex = newLista.findIndex((resp) => resp.gasto === gasto && resp.cantidad === cantidad);
      newLista.splice(buscarIndex, 1);
      setLista(newLista);
      setRestantePresupuesto(parseFloat(restantePresupuesto) + parseFloat(cantidad));
      setMsjGastoExcedido(false);
   };

   return (
      <>
         {/* <NavBar name="operacion" /> */}
         <br />
         <Container>
            <Grid>
               <Grid.Row columns={3}>
                  <Grid.Column>
                     <Header as="h3" icon textAlign="center">
                        <Icon name="user" circular />
                        <Header.Content>{presupuesto.nombre}</Header.Content>
                     </Header>
                  </Grid.Column>
                  <Grid.Column>
                     <Header as="h3" icon textAlign="center">
                        <Icon name="calendar" circular />
                        <Header.Content>{presupuesto.periodo}</Header.Content>
                     </Header>
                  </Grid.Column>
                  <Grid.Column>
                     <Header as="h3" icon textAlign="center">
                        <Icon name="money bill alternate outline" circular />
                        <Header.Content>${presupuesto.presupuestoInicial}</Header.Content>
                     </Header>
                  </Grid.Column>
               </Grid.Row>
            </Grid>
         </Container>

         <Container>
            <Grid>
               <Grid.Row columns={2}>
                  <Grid.Column>
                     <Segment textAlign="left">
                        <Container>
                           <Header as="h2" icon textAlign="center">
                              <Header.Content>Añade tus gastos aqui:</Header.Content>
                           </Header>
                           <Form>
                              <Form.Field>
                                 <label>Gasto:</label>
                                 <Popup
                                    content="Colocar el nombre del gasto."
                                    trigger={
                                       <input
                                          placeholder="Nombre Gasto"
                                          onChange={inputHandler}
                                          name="gasto"
                                          id="gastoNombre"
                                          value={data.gasto}
                                       />
                                    }
                                 />
                              </Form.Field>
                              <Form.Field>
                                 <label>Cantidad:</label>
                                 <Popup
                                    content="Colocar el importe del gasto ($)."
                                    trigger={
                                       <input
                                          type="number"
                                          placeholder="Importe"
                                          onChange={inputHandler}
                                          name="cantidad"
                                          id="gastoCantidad"
                                          value={data.cantidad}
                                       />
                                    }
                                 />
                              </Form.Field>
                              {presupuesto.presupuestoInicial ? (
                                 <div align="center">
                                    <Button basic color="blue" onClick={agregarGasto}>
                                       Agregar
                                    </Button>
                                 </div>
                              ) : (
                                 <div align="center">
                                    <Button basic color="blue" onClick={agregarGasto} disabled>
                                       Agregar
                                    </Button>
                                 </div>
                              )}
                           </Form>
                        </Container>
                     </Segment>
                  </Grid.Column>
                  <Grid.Column>
                     <Segment textAlign="left">
                        <Container>
                           <Table sortable celled fixed>
                              <Table.Header>
                                 <Table.Row>
                                    <Table.HeaderCell>Gasto</Table.HeaderCell>
                                    <Table.HeaderCell>Importe</Table.HeaderCell>
                                    <Table.HeaderCell>Acción</Table.HeaderCell>
                                 </Table.Row>
                              </Table.Header>
                              <Table.Body>{mostrarColumnas()}</Table.Body>
                           </Table>
                           {restantePresupuesto > presupuesto.presupuestoInicial * 0.5 ? (
                              <Message positive align="center">
                                 <Message.Header>
                                    <h3>Restante: ${restantePresupuesto}</h3>
                                 </Message.Header>
                              </Message>
                           ) : restantePresupuesto > presupuesto.presupuestoInicial * 0.25 ? (
                              <Message warning align="center">
                                 <Message.Header>
                                    <h3>Restante: ${restantePresupuesto}</h3>
                                 </Message.Header>
                              </Message>
                           ) : (
                              <Message error align="center">
                                 <Message.Header color="red">
                                    <h3>Restante: ${restantePresupuesto}</h3>
                                 </Message.Header>
                              </Message>
                           )}
                           <Message
                              align="center"
                              error
                              header="A excedido el presupuesto."
                              hidden={!msjGastoExcedido}
                           />

                           <div align="center">
                              <Button
                                 basic
                                 animated="vertical"
                                 color="red"
                                 className="submit"
                                 type="sybmit"
                                 onClick={onClickHandlerRetornarInicio}
                              >
                                 <Button.Content visible>Regresar al inicio</Button.Content>
                                 <Button.Content hidden>
                                    <Icon name="arrow left" />
                                 </Button.Content>
                              </Button>
                           </div>
                        </Container>
                     </Segment>
                  </Grid.Column>
               </Grid.Row>
            </Grid>
         </Container>

         <ModalAlert message={message} modal={modal} setModal={setModal} dispatchPresupuesto={dispatchPresupuesto} />
      </>
   );
};

export default Gastos;
