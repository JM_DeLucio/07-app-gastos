import React from "react";
import { useHistory } from "react-router-dom";
import { Button, Icon, Modal } from "semantic-ui-react";

const ModalAlert = ({ message, modal, setModal, dispatchPresupuesto }) => {
   const { mensaje, accion } = message;
   const history = useHistory();

   const onClickHandlerAceptar = () => {
      dispatchPresupuesto({
         type: "ADD_PRESUPUESTO_TOTAL",
         nombre: "",
         periodo: "",
         presupuestoInicial: "",
      });
      history.replace("/");
   };

   switch (accion) {
      case "cancelar":
         return (
            <>
               <Modal basic centered={false} size={"large"} open={modal} onClose={() => setModal(false)}>
                  <Modal.Content>
                     <Modal.Description align="center">
                        <h1>
                           <Icon color="red" name="close" />
                           {mensaje}
                        </h1>
                     </Modal.Description>
                  </Modal.Content>
                  <Modal.Actions align="center">
                     <Button color="red" onClick={() => setModal(false)}>
                        Cancelar
                     </Button>
                     <Button color="green" onClick={() => onClickHandlerAceptar()}>
                        Aceptar
                     </Button>
                  </Modal.Actions>
               </Modal>
            </>
         );
      default:
         return (
            <>
               <Modal basic centered={false} size={"large"} open={modal} onClose={() => setModal(false)}>
                  <Modal.Content>
                     <Modal.Description align="center">
                        <h1>
                           <Icon color="red" name="close" />
                           {mensaje}
                        </h1>
                     </Modal.Description>
                  </Modal.Content>
                  <Modal.Actions align="left">
                     <Button color="green" onClick={() => setModal(false)}>
                        Aceptar
                     </Button>
                  </Modal.Actions>
               </Modal>
            </>
         );
   }
};

export default ModalAlert;
