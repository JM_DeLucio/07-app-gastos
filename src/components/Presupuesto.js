import React, { useState, useContext } from "react";
import { useHistory } from "react-router-dom";
import { Button, Container, Form, Segment, Grid, Popup, Header } from "semantic-ui-react";
import PresupuestoContext from "../context/PresupuestoContext";

const Presupuesto = () => {
   const history = useHistory();
   const { dispatchPresupuesto } = useContext(PresupuestoContext);

   const [data, setData] = useState({
      nombre: "",
      periodo: "",
      presupuestoInicial: "",
   });

   const inputHandlerPresupuesto = (e) => {
      if (e.target.name === "presupuestoInicial") {
         const str = e.target.value.replace(/[^0-9.]/g, "");
         if (str.length <= 9) {
            str.replace(/[^0-9.]/g, "");
            setData((prevState) => ({
               ...prevState,
               [e.target.name]: str.toUpperCase(),
            }));
         }
      } else {
         const str = e.target.value.replace(/[^a-zA-Z0-9 ]/g, "");
         if (str.length <= 25) {
            str.replace(/[^a-zA-Z0-9 ]/g, "");
            setData((prevState) => ({
               ...prevState,
               [e.target.name]: str.toUpperCase(),
            }));
         }
      }
   };

   const onClickHandler = () => {
      dispatchPresupuesto({
         type: "ADD_PRESUPUESTO_TOTAL",
         nombre: data.nombre,
         periodo: data.periodo,
         presupuestoInicial: data.presupuestoInicial,
      });
      history.replace("/operacion");
   };

   const limpiarPresupuesto = () => {
      setData({
         nombre: "",
         periodo: "",
         presupuestoInicial: "",
      });
   };

   return (
      <>
         {/* <NavBar name="inicio" /> */}
         <Grid textAlign="center" style={{ height: "90vh" }} verticalAlign="middle">
            <Grid.Column style={{ maxWidth: 500 }}>
               <Header as="h2" color="black" textAlign="center">
                  APP DEL PRESUPUESTO
               </Header>
               <Segment>
                  <br />
                  <Container>
                     <Grid>
                        <Grid.Row>
                           <Grid.Column>
                              <Form>
                                 <Form.Field align="left">
                                    <label>Nombre:</label>
                                    <Popup
                                       content="Colocar su nombre."
                                       trigger={
                                          <input
                                             fluid
                                             type="text"
                                             name="nombre"
                                             placeholder="Nombre"
                                             value={data.nombre}
                                             onChange={inputHandlerPresupuesto}
                                          />
                                       }
                                    />
                                 </Form.Field>
                                 <Form.Field align="left">
                                    <label>Periodo:</label>
                                    <Popup
                                       content="Colocar el periodo que cubre su presupuesto."
                                       trigger={
                                          <input
                                             fluid
                                             type="text"
                                             name="periodo"
                                             placeholder="Periodo"
                                             value={data.periodo}
                                             onChange={inputHandlerPresupuesto}
                                          />
                                       }
                                    />
                                 </Form.Field>
                                 <Form.Field align="left">
                                    <label>Presupuesto:</label>
                                    <Popup
                                       content="Colocar su presupuesto ($)."
                                       trigger={
                                          <input
                                             type="number"
                                             id="nuberId"
                                             name="presupuestoInicial"
                                             placeholder="Presupuesto"
                                             value={data.presupuestoInicial}
                                             onChange={inputHandlerPresupuesto}
                                          />
                                       }
                                    />
                                 </Form.Field>
                              </Form>
                              <br />
                              <br />
                              <Container>
                                 {data.presupuestoInicial.length === 0 ||
                                 data.periodo.length === 0 ||
                                 data.nombre.length === 0 ? (
                                    <Popup
                                       content="Para iniciar se necesitan llenar todos los campos."
                                       trigger={
                                          <Button basic color="green" onClick={onClickHandler} disabled>
                                             Iniciar
                                          </Button>
                                       }
                                    />
                                 ) : (
                                    <Button basic color="green" onClick={onClickHandler}>
                                       Iniciar
                                    </Button>
                                 )}
                                 <Button
                                    basic
                                    color="red"
                                    onClick={() => {
                                       limpiarPresupuesto();
                                    }}
                                 >
                                    Limpiar
                                 </Button>
                              </Container>
                           </Grid.Column>
                        </Grid.Row>
                     </Grid>
                  </Container>
               </Segment>
            </Grid.Column>
         </Grid>
      </>
   );
};

export default Presupuesto;
