import React, { useReducer } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "semantic-ui-css/semantic.min.css";
import "./App.css";

import Presupuesto from "./components/Presupuesto";
import Gastos from "./components/Gastos";

import PresupuestoContext from "./context/PresupuestoContext";
import PresupuestoReducer from "./reducers/PresupuestoReducer";
const initPresupuestoTotal = () => {
   return {
      nombre: "",
      periodo: "",
      presupuestoInicial: "",
   };
};

export const App = () => {
   const [presupuesto, dispatchPresupuesto] = useReducer(PresupuestoReducer, {}, initPresupuestoTotal);

   return (
      <PresupuestoContext.Provider value={{ presupuesto, dispatchPresupuesto }}>
         <Router>
            <div align="center">
               <Route path="/" exact component={Presupuesto} />
               <Route path="/operacion" exact component={Gastos} />
            </div>
         </Router>
      </PresupuestoContext.Provider>
   );
};
